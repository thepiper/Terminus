Quat is this.
----------

Terminus is an IoT system. It dreams of becoming *the* IoT system - real security, infinite forward compatibility, machine learning integration. Something its family can be proud of. It dreams of the day when mama Bluetooth and papa IFTTT can pat it on its back and send it off to big boy school.

But right now it's kind of shit.

License
----------

MIT license.